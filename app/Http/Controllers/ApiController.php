<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class ApiController extends Controller
{

    public function create(Request $request){
        // dd($request->all());
        $create = Student::create([
            'class_id'=>$request->class_id,
            'book_id'=>$request->book_id,
            'subject_id'=>$request->subject_id,
            'teacher_id'=>$request->teacher_id,
            'name'=>$request->name,
            'lname'=>$request->lname,
            'address'=>$request->address,
            'country'=>$request->country,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'birthday'=>$request->birthday,
            'gender'=>$request->gender,
        ]);
        if($create){
            return response()->json([
                'status'=>200,
                'done'=>'Student Has Been Created Successfully'
            ]);
        }
    }

    public function show(){
        $data = Student::with(['class','book','subject','teacher'])->get();
        // dd($data);
        return response()->json([
            'status'=>200,
            'data' => $data
        ]);
    }

    public function delete($id ){

            $delete=Student::where('id',$id)->delete();
            if ($delete) {
                return response()->json([
                    'status'=>200,
                    'delete'=>'Student Has Been Deleted'
                ]);
            }
    }

    public function update(Request $request ){
        // dd($request->all());
        $updte = Student::where('id',$request->stdid)->update([
            'class_id'=>$request->class_id,
            'book_id'=>$request->book_id,
            'subject_id'=>$request->subject_id,
            'teacher_id'=>$request->teacher_id,
            'name'=>$request->name,
            'lname'=>$request->lname,
            'address'=>$request->address,
            'country'=>$request->country,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'birthday'=>$request->birthday,
            'gender'=>$request->gender,
        ]);
        if($updte){
            return response()->json([
                'status'=>200,
                'update'=>'Student Has Been Updated Successfully'
            ]);
        }
    }
}
