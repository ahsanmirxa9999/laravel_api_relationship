<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClassModel;
use App\Models\Book;
use App\Models\Subject;
use App\Models\Teacher;

class Student extends Model
{
    use HasFactory;
    protected $table = 'student';
    protected $primarykey = "id";
    protected $guarded = [];

    public function class()
    {
        return $this->belongsTo(ClassModel::class, 'class_id', 'class_id');
    }
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id', 'book_id');
    }
    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'subject_id');
    }
    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'teacher_id');
    }
}
