<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ViewController;

Route::get('/',[ViewController::class,'index']);
Route::get('/view_student',[ViewController::class,'view']);
Route::get('/edit/{id}',[ViewController::class,'edit']);
