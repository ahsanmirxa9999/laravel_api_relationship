<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class ViewController extends Controller
{
    public function index()
    {
        return view('/form');
    }

    public function view(){
        return view('/table');
    }
    public function edit($id){
        $data=Student::where('id',$id)->with(['class','book','subject','teacher'])->first();
        // dd($data);
        return view('Update_form')->with(compact('data'));
    }
}
