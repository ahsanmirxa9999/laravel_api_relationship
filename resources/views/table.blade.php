<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student Management System</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"
        rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

</head>

<body>
    <div class="container" style="margin-top: 50px;">
        <h3 class="text-center text-danger"><b>Student Management System</b> </h3>
        <a href="{{ url('/') }}" class="btn btn-success mb-2">Add Student</a>

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Class Name</th>
                    <th>Book Name</th>
                    <th>Teacher Name</th>
                    <th>Subject Name</th>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Country</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Birthday</th>
                    <th>Gender</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody id="bodyData">

            </tbody>
        </table>
    </div>
</body>

</html>

<script>
    // ============== Delete Toastr ==================
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
</script>


<script>
    function getStudent(params) {
        $.ajax({
            url: "http://127.0.0.1:8000/api/getStudent",
            method: "GET",
            success: function(res) {

                let row = "";
                res.data.forEach((element, index) => {
                    console.log(element)
                    row = row + `
                    <tr>
                        <td>${index+1}</td>
                        <td>${element.class.class_name}</td>
                        <td>${element.book.book_name}</td>
                        <td>${element.teacher.teacher_name}</td>
                        <td>${element.subject.subject_name}</td>
                        <td>${element.name}</td>
                        <td>${element.lname}</td>
                        <td>${element.address}</td>
                        <td>${element.country}</td>
                        <td>${element.phone}</td>
                        <td>${element.email}</td>
                        <td>${element.birthday}</td>
                        <td>${element.gender}</td>
                        <td><a href="{{ url('/edit/${element.id}') }}"><button class='btn btn-success'>Update</button></a></td>
                        <td><button class='btn btn-danger' onclick="deletefun(${element.id})">Delete</button></td>
                    </tr>
                    `
                });
                document.getElementById('bodyData').innerHTML = row
            }
        });
    }
    getStudent();

    function deletefun(id) {
        $.ajax({
            url: "http://127.0.0.1:8000/api/delete/" + id,
            method: "GET",
            success: function(res) {
                if (res.delete) {
                    getStudent();
                    toastr.error(res.delete,'Data!',{timeOut:2000})
                    // alert(res.delete);
                } else {
                    alert('Not deleted');
                }
            }

        });
    }
</script>
